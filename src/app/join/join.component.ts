import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { User } from '../login/user';
import { JoinService } from './join.service';

@Component({
  selector: 'app-join',
  templateUrl: './join.component.html',
  styleUrls: ['./join.component.css']
})
export class JoinComponent implements OnInit {

  us:User = new User();

  constructor(private _js:JoinService) {

  }

  ngOnInit() {
  }

  doJoin():void{
    if(!this.us.uiId){
      alert('아이디를 입력해주세요');
      return;
    }
    if(!this.us.uiPwd){
      alert('비밀번호를 입력해주세요');
      return;
    }
    if(!this.us.uiAge){
      alert('나이를 입력해주세요');
      return;
    }

    this._js.doJoin(this.us).subscribe(res=>{
      if(res.response){
        this.us = res.response;
        alert('회원가입에 성공하였습니다.');
      }else{
        alert('회원가입에 실패했습니다. 다시 한번 확인해주세요');
      }
      console.log(res);
    });

  }

}
