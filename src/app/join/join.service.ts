import { Injectable } from '@angular/core';
import { User } from '../login/user';
import { ajax } from 'rxjs/ajax';
@Injectable({
  providedIn: 'root'
})
export class JoinService {

  private baseUrl:string = 'http://localhost:88';

  constructor() { }

  doJoin(us:User){
    return ajax.post(this.baseUrl + '/join',
    {'uiId':us.uiId,'uiPwd':us.uiPwd,'uiAge':us.uiAge},
    {'Content-Type':'application/json','rxjs-custom-header':'Rxjs'}
    );
  }
}
