import { Injectable } from '@angular/core';
import { Board } from './board';
import { ajax } from 'rxjs/ajax';

@Injectable({
  providedIn: 'root'
})
export class BoardService {
  private baseUrl:string = 'http://localhost:88/';
  constructor() { }

  getBoardInfoService(board:Board){
    return ajax.get(this.baseUrl + 'boardinfos');
  }
  getBoardInfo(biNum:number){
    return ajax.get(this.baseUrl + 'boardinfo?biNum=' + biNum);
  }
}