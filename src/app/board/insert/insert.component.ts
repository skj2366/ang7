import { Component, OnInit } from '@angular/core';
import { Board } from '../board';
import { InsertService } from './insert.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-insert',
  templateUrl: './insert.component.html',
  styleUrls: ['./insert.component.css']
})
export class InsertComponent implements OnInit {

  board:Board = new Board();
  constructor(private _is:InsertService, private _router:Router) {

  }

  ngOnInit() {
  }


  insertBoard(board:Board){ 
    this._is.insertBoard(this.board).subscribe(res=>{
      console.log(res.response);
      console.log(res);
      this.goPage('/board');
    })
  }

  goPage(url:string){
    this._router.navigate([url]);
  }
}