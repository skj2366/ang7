import { Injectable } from '@angular/core';
import { ajax } from 'rxjs/ajax';
import { Board } from '../board';

@Injectable({
  providedIn: 'root'
})
export class InsertService {
  private baseUrl:string = 'http://localhost:88/';
  constructor() { }

  insertBoard(board:Board){
    return ajax.post(this.baseUrl+'insert',board,
    {'Content-Type':'application/json','rxjs-custom-header':'Rxjs'});
  }
}
